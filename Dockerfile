FROM python:3.6-alpine
ARG PIPENV_VERSION==2018.11.26
RUN pip install pipdeptree pipenv==$PIPENV_VERSION

# Include gcc build env
RUN apk add --update \
  build-base \
  libffi-dev \
  musl-dev \
  openssl-dev \
  python3-dev \
  libxml2-dev \
  libxslt-dev \
  && rm -rf /var/cache/apk/*

COPY analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
