package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	scannercli "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/cli"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2/pkgmngr"
)

const (
	flagDependencyPath    = "pip-dependency-path"
	// PIP_DEPENDENCY_PATH is deprecated since GitLab 12.2 and should be removed in GitLab 13.0
	flagEnvDependencyPath = "DS_PIP_DEPENDENCY_PATH,PIP_DEPENDENCY_PATH"
)

func analyzeFlags() []cli.Flag {
	return append(
		scannercli.ClientFlags(),
		cli.StringFlag{
			Name:   flagDependencyPath,
			EnvVar: flagEnvDependencyPath,
			Usage:  "Provide a path to pre-fetched dependencies to skip retrieval",
			Value:  "",
		},
	)
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// parse args and make new client
	client, err := scannercli.NewClient(c)
	if err != nil {
		return nil, err
	}

	opts := pkgmngr.InstallOptions{
		CacheDir:  "./dist",
		SkipFetch: false,
	}

	// If dependency path is passed explicitly, skip fetching and rely on cache
	if c.String(flagDependencyPath) != "" {
		opts = pkgmngr.InstallOptions{
			CacheDir:  c.String(flagDependencyPath),
			SkipFetch: true,
		}
	}

	pm, err := pkgmngr.NewPackageManager(path)
	if err != nil {
		return nil, err
	}

	if err := pm.InstallDependencies(opts); err != nil {
		return nil, err
	}

	input, err := pm.DependencyGraph()
	if err != nil {
		return nil, err
	}

	// parse input, get parsed file
	buf := bytes.NewBuffer(input)
	source, err := scanner.Parse(buf, "pipdeptree.json", pm.DepFile())
	if err != nil {
		return nil, err
	}

	// get advisories
	advisories, err := client.Advisories(source.Packages())
	if err != nil {
		return nil, err
	}

	// return affected sources
	var output bytes.Buffer
	result := scanner.AffectedSources(advisories, *source)
	enc := json.NewEncoder(&output)
	enc.SetIndent("", "  ")
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}
