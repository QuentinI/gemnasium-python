package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	switch info.Name() {
	case "requirements.txt", "requirements.pip", "Pipfile", "requires.txt", "setup.py":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("gemnasium-python", Match)
}
